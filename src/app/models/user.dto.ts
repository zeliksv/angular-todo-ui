export interface UserDto {
  id: string;
  email: string;
  first_name: string;
  last_name: string;
  todos: string[];
  disabled: boolean;
}
