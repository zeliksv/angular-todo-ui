import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, of} from 'rxjs';

import {TodoItemDto} from "../models/todo-item.dto";
import {UserService} from "./user.service";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private apiUrl = `${environment.apiUrl}/todos`;

  constructor(private http: HttpClient, private userService: UserService) { }

  getTodos(): Observable<TodoItemDto[]> {
    const userId = this.userService.getUser()?.id;

    if (userId) {
      return this.http.post<TodoItemDto[]>(`${this.apiUrl}-by-user`, {userId});
    }

    return of([]);
  }

  getTodoById(id: string): Observable<TodoItemDto | null> {
    return this.http.get<TodoItemDto | null>(`${this.apiUrl}/${id}`);
  }

  editTodoById(id: string, todoItem: TodoItemDto): Observable<TodoItemDto> {
    return this.http.put<TodoItemDto>(`${this.apiUrl}/${id}`, {...todoItem});
  }

  createTodoItem(todoItem: TodoItemDto): Observable<TodoItemDto | null> {
    const userId = this.userService.getUser()?.id;

    if (userId) {
      return this.http.post<TodoItemDto>(`${this.apiUrl}/new`, {...todoItem, userId});
    }

    return of(null);
  }

  removeTodoById(id: string): Observable<Record<string, string>> {
    return this.http.delete<Record<string, string>>(`${this.apiUrl}/${id}`);
  }
}
