import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router} from "@angular/router";
import { map } from 'rxjs/operators';
import {of} from "rxjs";

import {UserDto} from "../models/user.dto";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: UserDto | null = null;
  private apiUrl = `${environment.apiUrl}/user`;

  constructor(private http: HttpClient, private router: Router) { }

  getMe(email: string) {
    return this.http.post<UserDto | null>(`${this.apiUrl}/me`, { email }).pipe(
      map(
        (user) => {
          this.user = user;

          if (user) {
            this.router.navigateByUrl('');

            return user;
          }

          return of(null)
        }
      )
    );
  }

  getUser() {
    return this.user;
  }
}
