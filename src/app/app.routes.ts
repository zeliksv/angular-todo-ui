import { Routes } from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {EditTodoComponent} from "./pages/edit-todo/edit-todo.component";
import {CreateTodoComponent} from "./pages/create-todo/create-todo.component";
import {LoginComponent} from "./pages/login/login.component";

export const routes: Routes = [
  { title: 'Todo List', path: '' , component: HomeComponent},
  { title: 'Login', path: 'login' , component: LoginComponent},
  { title: 'Create New Todo Item', path: 'todo/new', component: CreateTodoComponent},
  { title: 'Edit Todo Item', path: 'todo/:id', component: EditTodoComponent}
];
