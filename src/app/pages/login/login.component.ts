import { Component } from '@angular/core';

import {LoginFormComponent} from "../../components/login-form/login-form.component";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    LoginFormComponent
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  constructor(private userService: UserService) {
  }
  onLoginUser(email: string): void {
    this.userService.getMe(email).subscribe();
  }
}
