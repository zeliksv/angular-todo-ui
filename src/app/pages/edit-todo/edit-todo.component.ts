import { Component } from '@angular/core';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import {CommonModule} from "@angular/common";
import {ActivatedRoute, Params, Router} from '@angular/router';

import {TodoFormComponent} from "../../components/todo-form/todo-form.component";
import {TodoItemDto} from "../../models/todo-item.dto";
import {TodoService} from "../../services/todo.service";

@Component({
  selector: 'app-edit-todo',
  standalone: true,
  imports: [TodoFormComponent, NzSpinModule, CommonModule],
  templateUrl: './edit-todo.component.html',
  styleUrl: './edit-todo.component.scss'
})
export class EditTodoComponent {
  constructor(private route: ActivatedRoute, private todoService: TodoService, private router: Router) { }

  defaultTodo: TodoItemDto | null = null;
  loading: boolean = true;

  onSaveTodo(todo: TodoItemDto): void {
    this.todoService.editTodoById(todo.id, todo).subscribe((data) => {
      this.router.navigate(['/']);
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const id = params['id'];

      this.loading = true;

      this.todoService.getTodoById(id).subscribe((data) => {
        this.defaultTodo = data;
        this.loading = !data;
      });
    });
  }
}
