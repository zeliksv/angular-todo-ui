import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { v4 as uuidv4 } from 'uuid';

import {TodoFormComponent} from "../../components/todo-form/todo-form.component";
import {TodoItemDto} from "../../models/todo-item.dto";
import {TodoService} from "../../services/todo.service";

@Component({
  selector: 'app-create-todo',
  standalone: true,
  imports: [TodoFormComponent],
  templateUrl: './create-todo.component.html',
  styleUrl: './create-todo.component.scss'
})
export class CreateTodoComponent {
  constructor(private todoService: TodoService, private router: Router) { }

  defaultTodo: TodoItemDto = {id: uuidv4(), title: '', description: '', createdAt: Date.now().toString(), userId: ''};

  onSaveTodo(todo: TodoItemDto): void {
    this.todoService.createTodoItem(todo).subscribe(() => {
      this.router.navigate(['/']);
    });
  }
}
