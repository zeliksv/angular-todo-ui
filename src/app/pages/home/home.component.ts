import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';

import {TodoListComponent} from "../../components/todo-list/todo-list.component";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [NzButtonModule, NzIconModule, TodoListComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {
  constructor(private router: Router, private userService: UserService) {}

  createTodoItem(): void {
    this.router.navigate(['/todo/new']);
  }

  ngOnInit() {
    const userData = this.userService.getUser();

    if(!userData) {
      this.router.navigate(['/login']);
    }
  }
}
