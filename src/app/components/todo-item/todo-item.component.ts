import {Component, EventEmitter, Input, Output} from '@angular/core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {Router} from "@angular/router";

import {TodoItemDto} from "../../models/todo-item.dto";

@Component({
  selector: 'app-todo-item',
  standalone: true,
  imports: [NzIconModule],
  templateUrl: './todo-item.component.html',
  styleUrl: './todo-item.component.scss'
})
export class TodoItemComponent {
  constructor(private router: Router) {}

  @Input() todoItem: TodoItemDto | null = null

  @Output() deleteTodo: EventEmitter<string> = new EventEmitter<string>();

  onDeleteTodo(): void {
    this.deleteTodo.emit(this.todoItem?.id);
  }

  editTodoItem(): void {
    this.router.navigate([`/todo/${this.todoItem?.id}`]);
  }
}
