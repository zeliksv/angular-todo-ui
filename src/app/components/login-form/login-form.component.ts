import {Component, EventEmitter, Output} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzInputModule} from "ng-zorro-antd/input";

@Component({
  selector: 'app-login-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, NzFormModule, NzButtonModule, NzInputModule],
  templateUrl: './login-form.component.html',
  styleUrl: './login-form.component.scss'
})
export class LoginFormComponent {
  email: string = '';

  @Output() onLogin: EventEmitter<string> = new EventEmitter();

  loginForm = new FormGroup({
    email: new FormControl(this.email),
  });

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl(this.email)
    });
  }

  login(event: Event) {
    event.preventDefault();


    if(this.loginForm.value.email) {
      this.onLogin.emit(this.loginForm.value.email);
    }
  }
}
