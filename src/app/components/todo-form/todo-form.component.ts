import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup, ReactiveFormsModule, FormControl} from "@angular/forms";
import {CommonModule} from "@angular/common";
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';

import {TodoItemDto} from "../../models/todo-item.dto";
import {v4 as uuidv4} from "uuid";

@Component({
  selector: 'app-todo-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, NzFormModule, NzButtonModule, NzInputModule],
  templateUrl: './todo-form.component.html',
  styleUrl: './todo-form.component.scss'
})
export class TodoFormComponent {
  @Input() defaultTodo: TodoItemDto | null = { id: uuidv4(), title: '', description: '', createdAt: Date.now().toString(), userId: ''};
  @Output() saveTodo: EventEmitter<TodoItemDto> = new EventEmitter<TodoItemDto>();

  form = new FormGroup({
    title: new FormControl(this.defaultTodo?.title),
    description: new FormControl(this.defaultTodo?.description),
  });

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(this.defaultTodo?.title),
      description: new FormControl(this.defaultTodo?.description),
    });
  }

  onSubmit(event: Event): void {
    event.preventDefault();

    this.saveTodo.emit({
      id: this.defaultTodo?.id ?? uuidv4(),
      title: this.form.value.title ?? '',
      description: this.form.value.description ?? '',
      createdAt: this.defaultTodo?.createdAt ?? Date.now().toString(),
      userId: this.defaultTodo?.userId ?? ''
    });

    this.form.reset();
  }
}
