import {orderBy} from "lodash";
import { Component } from '@angular/core';
import {CommonModule} from "@angular/common";
import { ActivatedRoute } from '@angular/router';

import {TodoItemComponent} from "../todo-item/todo-item.component";
import {TodoService} from "../../services/todo.service";
import {TodoItemDto} from "../../models/todo-item.dto";

@Component({
  selector: 'app-todo-list',
  standalone: true,
  imports: [CommonModule, TodoItemComponent],
  templateUrl: './todo-list.component.html',
  styleUrl: './todo-list.component.scss'
})
export class TodoListComponent {
  todos: TodoItemDto[] = []

  constructor(private route: ActivatedRoute, private todoService: TodoService) { }

  onDeleteTodo(id: string): void {
    this.todoService.removeTodoById(id).subscribe(() => {
        this.todoService.getTodos().subscribe(todos => {
          this.todos = todos;
        });
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(() => {
        this.todoService.getTodos().subscribe(todos => {
          this.todos = orderBy(todos, 'createdAt', 'desc');
        });
    });
  }
}
